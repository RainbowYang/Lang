package moe.gogo.compiler

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec

class TokensTest : StringSpec() {

    init {
        "Lexicon"{
            Tokens.IF.matches("if") shouldBe true
            Tokens.ELSE.matches("else") shouldBe true
            Tokens.FOR.matches("for") shouldBe true

            Tokens.AND.matches("&&") shouldBe true
            Tokens.OR.matches("||") shouldBe true
            Tokens.NOT.matches("!") shouldBe true
            Tokens.LESS_EQ.matches("<=") shouldBe true
            Tokens.LESS.matches("<") shouldBe true
            Tokens.GREATER_EQ.matches(">=") shouldBe true
            Tokens.GREATER.matches(">") shouldBe true
            Tokens.EQUAL.matches("==") shouldBe true
            Tokens.ASSIGN.matches("=") shouldBe true
            Tokens.PLUS.matches("+") shouldBe true
            Tokens.MINUS.matches("-") shouldBe true
            Tokens.TIMES.matches("*") shouldBe true
            Tokens.DIV.matches("/") shouldBe true
            Tokens.LEFT_PH.matches("(") shouldBe true
            Tokens.RIGHT_PH.matches(")") shouldBe true
            Tokens.LEFT_BK.matches("[") shouldBe true
            Tokens.RIGHT_BK.matches("]") shouldBe true
            Tokens.LEFT_BR.matches("{") shouldBe true
            Tokens.RIGHT_BR.matches("}") shouldBe true
            Tokens.DOT.matches(".") shouldBe true
            Tokens.COMMA.matches(",") shouldBe true
            Tokens.COLON.matches(":") shouldBe true
            Tokens.SEMICOLON.matches(";") shouldBe true

            Tokens.COMMENTS.matches("//") shouldBe true
            Tokens.MULTILINE_COMMENTS.matches("/*") shouldBe true
            Tokens.DOCUMENT_COMMENTS.matches("/**") shouldBe true

            Tokens.NUMBER.matches("1948965") shouldBe true
            Tokens.ID.matches("a12345") shouldBe true
        }
    }

}