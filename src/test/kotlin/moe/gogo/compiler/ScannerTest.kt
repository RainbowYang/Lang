package moe.gogo.compiler

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec

class ScannerTest : StringSpec() {

    init {
        val scanner = Scanner(Tokens.LEXICON)
        "multi space"{
            scanner.read("1 + 3   *     32").size shouldBe 5
        }
        "scan"{
            scanner.read("""
                | a = 1 + 3 * 32 // this is a comment
                | /*this is a
                | multiline
                | comment*/a > 10
            """.trimMargin()).size shouldBe 12
        }
    }

}