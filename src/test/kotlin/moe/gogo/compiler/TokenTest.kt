package moe.gogo.compiler

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec
import java.util.ArrayDeque
import java.util.Deque

class TokenTest : StringSpec() {

    init {
        "NameToken"{
            val token = NameToken("id", "name", Regex("abc"))
            token.id shouldBe "id"
            token.name shouldBe "name"
            token.regex.matches("abc") shouldBe true
            token.toLexeme("abc").toString() shouldBe "<name>"
        }
        "ArgumentToken"{
            val token = ArgumentToken("a_number", Regex("""a\d+"""))
            token.toLexeme("a123").toString() shouldBe "<a_number, a123>"
        }
        "putLexeme"{
            val token = ArgumentToken("a_number", Regex("""a\d+"""))
            val deque = ArrayDeque<String>()
            val list = mutableListOf<Lexeme>()
            token.putLexeme(list, deque, "a123")
            list[0].toString() shouldBe "<a_number, a123>"
        }
        "MultiWordsToken" {
            val token = MultiWordsToken("Comment", Regex("""//"""), Regex("""\n"""))
            val str = "//Some comments here and\nthere will remind"
            val deque = splitString(str)
            val list = mutableListOf<Lexeme>()



            token.putLexeme(list, deque, deque.pop())
            list[0].toString() shouldBe "<Comment, Some comments here and>"
            deque.reduce { acc, s -> acc + s } shouldBe "there will remind"
        }
    }

    private fun splitString(string: String): Deque<String> {
        val split = Regex("""\S+|[\s&&[^\r\n]]+|\n""")
        val lexemes = split.findAll(string).map { it.value }
        return ArrayDeque(lexemes.toList())
    }

}