package moe.gogo.compiler


abstract class Lexeme(val token: Token) {

    override fun toString(): String = "<$token>"

}

class NameLexeme(token: Token) : Lexeme(token)

class ArgumentLexeme(token: Token, private val argument: String) : Lexeme(token) {

    override fun toString(): String = "<$token, $argument>"

}