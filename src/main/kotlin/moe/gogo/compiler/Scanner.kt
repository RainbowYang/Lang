package moe.gogo.compiler

import java.util.ArrayDeque
import java.util.Deque

class Scanner(val lexicon: Lexicon) {

    fun read(string: String): List<Lexeme> {
        val list = mutableListOf<Lexeme>()

        val deque = splitString(string)
        convertToLexeme(deque, list)

        return list
    }

    private fun splitString(string: String): Deque<String> {
        val split = Regex("""\S+|[\s&&[^\r\n]]+|\n""")
        val lexemes = split.findAll(string).map { it.value }
        return ArrayDeque(lexemes.toList())
    }

    private fun convertToLexeme(deque: Deque<String>, list: MutableList<Lexeme>) {
        while (!deque.isEmpty()) {
            val it = deque.pop()
            if (it.isNotEmpty() && it.isNotBlank()) {
                val token: Token? = selectToken(it)
                token?.putLexeme(list, deque, it)
            }
        }
    }

    fun read(lines: List<String>): List<Lexeme> {
        val builder = StringBuilder()
        lines.forEach {
            builder.append(it)
            builder.append("\n")
        }
        return read(builder.toString())
    }

    private fun selectToken(string: String): Token? {
        lexicon.tokens.forEach {
            if (it.matches(string)) {
                return it
            }
        }
        return null
    }
}